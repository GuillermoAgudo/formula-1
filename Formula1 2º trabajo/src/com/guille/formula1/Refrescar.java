package com.guille.formula1;

/**
 * Created by Guille on 08/12/2015.
 */
public class Refrescar extends Thread {
    private Formula1 f1;


    public Refrescar(Formula1 f1){
        this.f1 = f1;
    }

    public void run()
    {
        while (isAlive())
        {
            if (f1.tfBuscarEscuderia.getText().isEmpty())
            {
                f1.listarEscuderias();
            }

            if (f1.tfBuscarCoche.getText().isEmpty())
            {
                f1.listarCoches();
            }

            if (f1.tfBuscarPiloto.getText().isEmpty())
            {
                f1.listarPilotos();
            }

            try
            {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
