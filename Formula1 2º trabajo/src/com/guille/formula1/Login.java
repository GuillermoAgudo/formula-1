package com.guille.formula1;

import javax.swing.*;
import java.awt.event.*;

public class Login extends JDialog implements ActionListener{
    private JPanel contentPane;
    private JButton btAceptar;
    private JButton btCancelar;
    private JTextField tfUsuario;
    private JPasswordField tfContrasena;

    private String usuario;
    private String contrasena;

    public Login() {
        super();
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(btAceptar);
        setTitle("Login");
        pack();
        setLocationRelativeTo(null);
        setModal(true);

        btAceptar.addActionListener(this);
        btCancelar.addActionListener(this);

    }



    public static void main(String[] args) {
        Login dialog = new Login();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource() == btAceptar){
            aceptar();
        }

        if (e.getSource() == btCancelar){
            cancelar();
        }
    }

    private void aceptar() {
        if ((tfUsuario.getText().equals(""))
                || (tfContrasena.getPassword().toString().equals(""))) {
            JOptionPane.showMessageDialog(null,
                    "Debes introducir usuario y contraseņa",
                    "Login", JOptionPane.ERROR_MESSAGE);
            return;
        }

        usuario = tfUsuario.getText();
        contrasena = String.valueOf(tfContrasena.getPassword());
        setVisible(false);
    }

    private void cancelar() {
        setVisible(false);
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }
}
