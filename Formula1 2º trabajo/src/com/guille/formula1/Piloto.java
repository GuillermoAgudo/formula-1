package com.guille.formula1;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by willi on 07/11/2015.
 */
public class Piloto implements Serializable {
    private static final long serialVersionUID = 1L;
    private String Nombre;
    private String Nacionalidad;
    private Date FechaNacimiento;
    private int Campeonatos;
    private float Peso;
    private int Altura;
    private String Escuderia;

    public String getEscuderia() {
        return Escuderia;
    }

    public void setEscuderia(String escuderia) {
        Escuderia = escuderia;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getNacionalidad() {
        return Nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        Nacionalidad = nacionalidad;
    }

    public Date getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        FechaNacimiento = fechaNacimiento;
    }

    public int getCampeonatos() {
        return Campeonatos;
    }

    public void setCampeonatos(int campeonatos) {
        Campeonatos = campeonatos;
    }

    public float getPeso() {
        return Peso;
    }

    public void setPeso(float peso) {
        Peso = peso;
    }

    public int getAltura() {
        return Altura;
    }

    public void setAltura(int altura) {
        Altura = altura;
    }

    @Override
    public String toString() {
        return Nombre;
    }
}
