package com.guille.formula1;

import java.io.Serializable;

/**
 * Created by willi on 07/11/2015.
 */
public class Coche implements Serializable {
    private static final long serialVersionUID = 1L;
    private String Nombre;
    private String Motor;
    private int Caballos;
    private String Carroceria;
    private float CosteDelCoche;
    private String Pilotos;

    public String getPilotos() {
        return Pilotos;
    }

    public void setPilotos(String pilotos) {
        Pilotos = pilotos;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getMotor() {
        return Motor;
    }

    public void setMotor(String motor) {
        Motor = motor;
    }

    public int getCaballos() {
        return Caballos;
    }

    public void setCaballos(int caballos) {
        Caballos = caballos;
    }

    public float getCosteDelCoche() {
        return CosteDelCoche;
    }

    public void setCosteDelCoche(float costeDelCoche) {
        CosteDelCoche = costeDelCoche;
    }

    public String getCarroceria() {
        return Carroceria;
    }

    public void setCarroceria(String carroceria) {
        Carroceria = carroceria;
    }

    @Override
    public String toString() {
        return Nombre +", caballos: "+ Caballos;
    }
}
