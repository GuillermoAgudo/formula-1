package com.guille.formula1;

import java.io.Serializable;
import java.util.Date;


/**
 * Created by willi on 07/11/2015.
 */
public class Escuderia implements Serializable {
    private static final long serialVersionUID = 1L;
    private String Nombre;
    private String Director;
    private String Pais;
    private Date FechaCreacion;
    private int NumeroTrabajadores;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDirector() {
        return Director;
    }

    public void setDirector(String director) {
        Director = director;
    }

    public String getPais() {
        return Pais;
    }

    public void setPais(String pais) {
        Pais = pais;
    }

    public Date getFechaCreacion() {
        return FechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        FechaCreacion = fechaCreacion;
    }

    public int getNumeroTrabajadores() {
        return NumeroTrabajadores;
    }

    public void setNumeroTrabajadores(int numeroTrabajadores) {
        NumeroTrabajadores = numeroTrabajadores;
    }

    @Override
    public String toString() {
        return Nombre +", " + ", Director: "+ Director;

    }
}
