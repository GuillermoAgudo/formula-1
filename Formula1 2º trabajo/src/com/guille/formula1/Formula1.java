package com.guille.formula1;

import com.guille.formula1.Util.Util;
import com.toedter.calendar.JDateChooser;
import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.sql.*;
import java.sql.Date;
import java.util.*;
import java.util.List;

import static com.guille.formula1.Util.Constantes.PATH;

/**
 * Created by willi on 28/10/2015.
 */
public class Formula1 implements KeyListener,ActionListener {
    public JFrame frame;
    private JPanel panel1;
    private JTabbedPane tabbedPane1;
    private JTextField tfNomEscuderia;
    private JTextField tfNomDirector;
    private JTextField tfPais;
    private JDateChooser dcFechaCreacion;
    private JTextField tfNumTrabajadores;
    private JButton btNuevoEsc;
    private JButton btGuardarEsc;
    private JButton btModificarEsc;
    private JButton btEliminarEsc;
    private JTextField tfNombrePiloto;
    private JComboBox cbEscuderia;
    private JTextField tfCampeonatos;
    private JTextField tfAltura;
    private JTextField tfPeso;
    public JTextField tfBuscarEscuderia;
    public JTextField tfBuscarPiloto;
    public JTextField tfBuscarCoche;
    private JButton btNuevoPiloto;
    private JButton btGuardarPiloto;
    private JButton btModificarPiloto;
    private JButton btEliminarPiloto;
    private JTextField tfNombreCoche;
    private JTextField tfMotor;
    private JTextField tfCaballos;
    private JComboBox cbPiloto;
    private JTextField tfNacionalidad;
    private JTextField tfCarroceria;
    private JTextField tfCosteCoche;
    private JButton btNuevoCoche;
    private JButton btGuardarCoche;
    private JButton btModificarCoche;
    private JButton btEliminarCoche;
    private JDateChooser dcFechanacimiento;
    private JTable tablaEscuderias;
    private JTable tablaPilotos;
    private JTable tablaCoches;
    private JComboBox cbBuscarCoche;
    private JComboBox cbBuscarPiloto;
    private JComboBox cbBuscarEscuderia;
    private boolean nuevaEscuderia;
    private boolean nuevoPiloto;
    private boolean nuevoCoche;
    private int idEscuderia;
    private int idPiloto;
    private int idCoche;

    private Connection conexion;
    private DefaultTableModel mtEscuderias;
    private DefaultTableModel mtPilotos;
    private DefaultTableModel mtCoches;


    public static void main(String[] args) {
        JFrame frame = new JFrame("Formula1");
        Formula1 formula1 = new Formula1();
        frame.setContentPane(formula1.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setMinimumSize(new Dimension(750, 500));
        frame.setResizable(true);
        frame.pack();

        frame.setVisible(true);


    }

    public Formula1() {

        añadircamposcb();
        añardirlisteners();
        initTablaEscuderias();
        initTablaPilotos();
        initTablaCoches();
        tablaEscuderias.setRowSelectionAllowed(true);
        tablaCoches.setRowSelectionAllowed(true);
        tablaPilotos.setRowSelectionAllowed(true);

        try {
            conectar();
            rol(login());
            listarEscuderias();
            listarCoches();
            listarPilotos();
            actualizarcbEscuderiasConsulta();
            actualizarcbPilotosConsulta();

        } catch (ClassNotFoundException cnfe) {
            JOptionPane.showMessageDialog(null,
                    "No se ha podido cargar el driver del SGBD",
                    "Conectar", JOptionPane.ERROR_MESSAGE);
        } catch (SQLException sqle) {
            JOptionPane.showMessageDialog(null,
                    "No se ha podido conectar con el servidor. Comprueba " +
                            "que está arrancado",
                    "Conectar", JOptionPane.ERROR_MESSAGE);
        }


        Refrescar refrescar = new Refrescar(this);
        refrescar.start();

    }

    private void rol(String rol)
    {
        if (rol != null) {
            if (rol.equalsIgnoreCase("administrador"))
            {
                setEdicionCoches(false);
                setEdicionPilotos(false);
                setEdicionEscuderias(false);
            }
            if (rol.equalsIgnoreCase("usuario")) {
                setEdicionCoches(false);
                setEdicionPilotos(false);
                setEdicionEscuderias(false);
                tablaEscuderias.setEnabled(false);
                tablaPilotos.setEnabled(false);
                tablaCoches.setEnabled(false);
                btNuevoEsc.setEnabled(false);
                btNuevoCoche.setEnabled(false);
                btNuevoPiloto.setEnabled(false);
            }
        }
    }

    private void añardirlisteners()
    {
        btNuevoEsc.addActionListener(this);
        btGuardarEsc.addActionListener(this);
        btEliminarEsc.addActionListener(this);
        btModificarEsc.addActionListener(this);
        btNuevoPiloto.addActionListener(this);
        btGuardarPiloto.addActionListener(this);
        btModificarPiloto.addActionListener(this);
        btEliminarPiloto.addActionListener(this);
        btNuevoCoche.addActionListener(this);
        btGuardarCoche.addActionListener(this);
        btModificarCoche.addActionListener(this);
        btEliminarCoche.addActionListener(this);
        tfBuscarEscuderia.addKeyListener(this);
        tfBuscarPiloto.addKeyListener(this);
        tfBuscarCoche.addKeyListener(this);

        tablaEscuderias.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {


                if (tablaEscuderias.isRowSelected(tablaEscuderias.getSelectedRow()))
                    idEscuderia = Integer.parseInt(tablaEscuderias.getValueAt(tablaEscuderias.getSelectedRow(), 0).toString());

                String consulta = "SELECT * FROM escuderia WHERE id = ?";

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(consulta);
                    sentencia.setInt(1, idEscuderia);
                    ResultSet resultado = sentencia.executeQuery();

                    if (resultado.next()) {
                        tfNomEscuderia.setText(resultado.getString("nombre"));
                        tfNomDirector.setText(resultado.getString("director"));
                        tfPais.setText(resultado.getString("pais"));
                        dcFechaCreacion.setDate(resultado.getDate("fecha_creacion"));
                        tfNumTrabajadores.setText(String.valueOf(resultado.getInt("numero_trabajadores")));

                    }
                    setEdicionEscuderias(true);
                    btNuevoEsc.setEnabled(true);


                } catch (SQLException sqle) {
                    JOptionPane.showMessageDialog(null, "", "",
                            JOptionPane.ERROR_MESSAGE);
                }


            }

        });
        tablaPilotos.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

                if (tablaPilotos.isRowSelected(tablaPilotos.getSelectedRow()))
                    idPiloto = Integer.parseInt(tablaPilotos.getValueAt(tablaPilotos.getSelectedRow(), 0).toString());

                String consulta = "SELECT * FROM piloto WHERE id = ?";

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(consulta);
                    sentencia.setInt(1, idPiloto);
                    ResultSet resultado = sentencia.executeQuery();

                    if (resultado.next()) {
                        tfNombrePiloto.setText(resultado.getString("nombre"));
                        tfNacionalidad.setText(resultado.getString("nacionalidad"));
                        cbEscuderia.setSelectedItem(resultado.getString("escuderia"));
                        dcFechanacimiento.setDate(resultado.getDate("fecha_nacimiento"));
                        tfCampeonatos.setText(String.valueOf(resultado.getInt("campeonatos")));
                        tfPeso.setText(String.valueOf(resultado.getInt("peso")));
                        tfAltura.setText(String.valueOf(resultado.getInt("altura")));

                    }
                    setEdicionPilotos(true);
                    btNuevoPiloto.setEnabled(true);

                } catch (SQLException sqle) {
                    JOptionPane.showMessageDialog(null, "", "",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        tablaCoches.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

                if (tablaCoches.isRowSelected(tablaCoches.getSelectedRow()))
                    idCoche = Integer.parseInt(tablaCoches.getValueAt(tablaCoches.getSelectedRow(), 0).toString());

                String consulta = "SELECT * FROM coche WHERE id = ?";

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(consulta);
                    sentencia.setInt(1, idCoche);
                    ResultSet resultado = sentencia.executeQuery();

                    if (resultado.next()) {
                        tfNombreCoche.setText(resultado.getString("nombre"));
                        tfMotor.setText(resultado.getString("motor"));
                        tfCaballos.setText(String.valueOf(resultado.getInt("caballos")));
                        cbPiloto.setSelectedItem(resultado.getString("piloto"));
                        tfCarroceria.setText(resultado.getString("carroceria"));
                        tfCosteCoche.setText(String.valueOf(resultado.getInt("coste_coche")));

                    }
                    setEdicionCoches(true);
                    btNuevoCoche.setEnabled(true);

                } catch (SQLException sqle) {
                    JOptionPane.showMessageDialog(null, "", "",
                            JOptionPane.ERROR_MESSAGE);
                }

            }
        });
    }
    private String login()
    {
        Login login = new Login();
        login.setVisible(true);

        String usuario = login.getUsuario();
        String contrasena = login.getContrasena();
        String rol = "";

        String sql = "SELECT nombre, rol FROM usuario WHERE " +
                "nombre = ? AND contrasena = SHA1(?)";


        try
        {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, usuario);
            sentencia.setString(2, contrasena);
            ResultSet resultado = sentencia.executeQuery();

            if (!resultado.next())
            {
                JOptionPane.showMessageDialog(null,
                        "Usuario/Contraseña incorrectos", "Login",
                        JOptionPane.ERROR_MESSAGE);
                return null;
            }
            rol = resultado.getString("rol");

        } catch (SQLException sqle)
        {
            JOptionPane.showMessageDialog(null, "" , "",
                    JOptionPane.ERROR_MESSAGE);
        }
        return rol;
    }

    private void conectar() throws ClassNotFoundException, SQLException
    {
        Class.forName("com.mysql.jdbc.Driver");
        // FIXME coger la información de conexion
        // de un fichero de configuracion (.properties)
        conexion = DriverManager.getConnection(
                "jdbc:mysql://localhost:3306/formula1accesodatos", "root", "mysql");
    }

    public void actionPerformed(ActionEvent e)
    {
        String actionCommand = ((JButton) e.getSource()).getActionCommand();

        switch (actionCommand)
        {
            case "GuardarEscuderia":

                String sql = "INSERT INTO escuderia (nombre, director, " +
                        "pais, fecha_creacion, numero_trabajadores) " +
                        "VALUES (?, ?, ?, ?, ?)";

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);
                    sentencia.setString(1, tfNomEscuderia.getText());
                    sentencia.setString(2, tfNomDirector.getText());
                    sentencia.setString(3, tfPais.getText());
                    sentencia.setDate(4, new Date(dcFechaCreacion.getDate().getTime()));
                    sentencia.setInt(5, Integer.valueOf(tfNumTrabajadores.getText()));

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al dar de alta", "Alta escuderia");
                }

                listarEscuderias();
                limpiarcajasEsc();
                actualizarcbEscuderiasConsulta();
                setEdicionEscuderias(false);

                break;

            case "NuevaEscuderia":

                nuevaEscuderia = true;
                limpiarcajasEsc();
                setEdicionEscuderias(true);
                btModificarEsc.setEnabled(false);
                btEliminarEsc.setEnabled(false);

                break;

            case "ModificarEscuderia":
                nuevaEscuderia = false;


                sql = "UPDATE escuderia SET nombre = ? , director = ?, pais = ?, fecha_creacion = ?, numero_trabajadores = ? WHERE id ="+ idEscuderia;

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);
                    sentencia.setString(1, tfNomEscuderia.getText());
                    sentencia.setString(2, tfNomDirector.getText());
                    sentencia.setString(3, tfPais.getText());
                    sentencia.setDate(4, new Date(dcFechaCreacion.getDate().getTime()));
                    sentencia.setInt(5, Integer.valueOf(tfNumTrabajadores.getText()));

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al modificar", "Modificar escuderia");
                }

                listarEscuderias();
                actualizarcbEscuderiasConsulta();
                limpiarcajasEsc();
                setEdicionEscuderias(false);

                break;

            case "EliminarEscuderia":

                if ((Util.mensajeConfirmacion("¿Estas Seguro?")) == JOptionPane.NO_OPTION) {
                    return;
                }

                sql = "DELETE FROM escuderia WHERE id = "+ idEscuderia;

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al eliminar", "Eliminar escuderia");
                }

                listarEscuderias();
                actualizarcbEscuderiasConsulta();
                limpiarcajasEsc();
                setEdicionEscuderias(false);

                break;

            case "GuardarPiloto":

                sql = "INSERT INTO piloto (nombre, nacionalidad, " +
                        "escuderia, fecha_nacimiento,campeonatos, peso, altura, id_escuderia) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

                int id_escuderia = 0;
                id_escuderia = consultaID("id","escuderia","nombre",(String) cbEscuderia.getSelectedItem());

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);
                    sentencia.setString(1, tfNombrePiloto.getText());
                    sentencia.setString(2, tfNacionalidad.getText());
                    sentencia.setString(3, (String) cbEscuderia.getSelectedItem());
                    sentencia.setDate(4, new Date(dcFechanacimiento.getDate().getTime()));
                    sentencia.setInt(5, Integer.parseInt(tfCampeonatos.getText()));
                    sentencia.setFloat(6, Float.parseFloat(tfPeso.getText()));
                    sentencia.setInt(7, Integer.parseInt(tfAltura.getText()));
                    sentencia.setInt(8, id_escuderia);

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al dar de alta", "Alta Piloto");
                }


                listarPilotos();
                limpiarcajasPiloto();
                actualizarcbPilotosConsulta();
                setEdicionPilotos(false);

                break;

            case "NuevoPiloto":

                nuevoPiloto = true;
                limpiarcajasPiloto();
                setEdicionPilotos(true);
                btModificarPiloto.setEnabled(false);
                btEliminarPiloto.setEnabled(false);
                break;

            case "ModificarPiloto":

                nuevoPiloto = false;

                sql = "UPDATE piloto SET nombre = ? , nacionalidad = ?, escuderia = ?, fecha_nacimiento = ?, campeonatos = ?, peso = ?, altura = ? WHERE id ="+ idPiloto;

                id_escuderia = 0;
                id_escuderia = consultaID("id","escuderia","nombre",(String) cbEscuderia.getSelectedItem());

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);
                    sentencia.setString(1, tfNombrePiloto.getText());
                    sentencia.setString(2, tfNacionalidad.getText());
                    sentencia.setString(3, (String) cbEscuderia.getSelectedItem());
                    sentencia.setDate(4, new Date(dcFechanacimiento.getDate().getTime()));
                    sentencia.setInt(5, Integer.valueOf(tfCampeonatos.getText()));
                    sentencia.setFloat(6, Float.valueOf(tfPeso.getText()));
                    sentencia.setInt(7, Integer.valueOf(tfAltura.getText()));
                    sentencia.setInt(8,id_escuderia);

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al modificar", "Modificar escuderia");
                    sqle.printStackTrace();
                }

                listarPilotos();
                limpiarcajasPiloto();
                actualizarcbPilotosConsulta();
                setEdicionPilotos(false);

                break;

            case "EliminarPiloto":

                if ((Util.mensajeConfirmacion("¿Estas Seguro?")) == JOptionPane.NO_OPTION) {
                    return;
                }

                sql = "DELETE FROM piloto WHERE id = "+ idPiloto;

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al eliminar", "Eliminar piloto");
                }

                listarPilotos();
                actualizarcbPilotosConsulta();
                limpiarcajasPiloto();
                setEdicionPilotos(false);

                break;

            case "GuardarCoche":

                sql = "INSERT INTO coche (nombre, motor, caballos, " +
                        "piloto, carroceria, coste_coche, id_piloto) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?)";

                int id_piloto = 0;
                id_piloto = consultaID("id","piloto","nombre",(String) cbPiloto.getSelectedItem());

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);
                    sentencia.setString(1, tfNombreCoche.getText());
                    sentencia.setString(2, tfMotor.getText());
                    sentencia.setString(3, String.valueOf(tfCaballos.getText()));
                    sentencia.setString(4, (String) cbPiloto.getSelectedItem());
                    sentencia.setString(5, tfCarroceria.getText());
                    sentencia.setString(6, String.valueOf(tfCosteCoche.getText()));
                    sentencia.setInt(7, id_piloto);
                  ;

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al dar de alta", "Alta coche");
                }

                listarCoches();
                limpiarcajasCoche();
                setEdicionCoches(false);

                break;

            case "NuevoCoche":
                nuevoCoche = true;
                limpiarcajasCoche();
                setEdicionCoches(true);
                btModificarCoche.setEnabled(false);
                btEliminarCoche.setEnabled(false);
                break;

            case "ModificarCoche":
                nuevoCoche = false;

                sql = "UPDATE coche SET nombre = ? , motor = ?, caballos = ?, piloto = ?, carroceria = ?, coste_coche = ? WHERE id ="+ idCoche;


                id_piloto = consultaID("id","piloto","nombre",(String) cbPiloto.getSelectedItem());

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);
                    sentencia.setString(1, tfNombreCoche.getText());
                    sentencia.setString(2, tfMotor.getText());
                    sentencia.setInt(3, Integer.valueOf(tfCaballos.getText()));
                    sentencia.setString(4, (String) cbPiloto.getSelectedItem());
                    sentencia.setString(5, tfCarroceria.getText());
                    sentencia.setFloat(6, Float.valueOf(tfCosteCoche.getText()));
                    sentencia.setInt(7,id_piloto);

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al modificar", "Modificar coche");
                }


                listarCoches();
                limpiarcajasCoche();
                setEdicionCoches(false);

                break;

            case "EliminarCoche":
                if ((Util.mensajeConfirmacion("¿Estas Seguro?")) == JOptionPane.NO_OPTION) {
                    return;
                }

                sql = "DELETE FROM coche WHERE id = "+ idCoche;

                try {
                    PreparedStatement sentencia = conexion.prepareStatement(sql);

                    sentencia.executeUpdate();

                } catch (SQLException sqle) {
                    Util.mensajeError("Error al eliminar", "Eliminar coche");
                }


                listarCoches();
                limpiarcajasCoche();
                setEdicionCoches(false);

                break;
        }
    }


    private int consultaID (String select, String table, String campo, String condicion){

        String sql = "SELECT "+ select + " FROM "+ table + " WHERE "+ campo + " = ?";

        int id = 0;
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, condicion);
            ResultSet resultado = sentencia.executeQuery();

            if (resultado.next()) id = resultado.getInt(select);

        }catch (SQLException e){
            e.printStackTrace();
        }
        return id;
    }

    private void initTablaEscuderias() {
        mtEscuderias = new DefaultTableModel();
        tablaEscuderias.setModel(mtEscuderias);
        mtEscuderias.addColumn("Id");
        mtEscuderias.addColumn("Nombre");
        mtEscuderias.addColumn("Director");
        mtEscuderias.addColumn("País");
        mtEscuderias.addColumn("Fecha de creación");
        mtEscuderias.addColumn("Numero de trabajadores");
    }

    private void initTablaPilotos()
    {
        mtPilotos = new DefaultTableModel();
        tablaPilotos.setModel(mtPilotos);
        mtPilotos.addColumn("Id");
        mtPilotos.addColumn("Nombre");
        mtPilotos.addColumn("Nacionalidad");
        mtPilotos.addColumn("Escuderia");
        mtPilotos.addColumn("Fecha de nacimiento");
        mtPilotos.addColumn("Campeonatos");
        mtPilotos.addColumn("Peso");
        mtPilotos.addColumn("Altura");
    }

    private void initTablaCoches()
    {
        mtCoches = new DefaultTableModel();
        tablaCoches.setModel(mtCoches);
        mtCoches.addColumn("Id");
        mtCoches.addColumn("Nombre");
        mtCoches.addColumn("Motor");
        mtCoches.addColumn("Caballos");
        mtCoches.addColumn("Piloto");
        mtCoches.addColumn("Carroceria");
        mtCoches.addColumn("Coste del coche");

    }

    public void listarEscuderias()
    {
        tablaEscuderias.clearSelection();
        mtEscuderias.setNumRows(0);

        String sql = "SELECT * FROM escuderia";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();

            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String director = resultado.getString("director");
                String pais = resultado.getString("pais");
                Date fechaCreacion = resultado.getDate("fecha_creacion");
                int numeroTrabajadores = resultado.getInt("numero_trabajadores");

                Object[] fila = new Object[]{id, nombre, director,
                        pais, fechaCreacion, numeroTrabajadores};

                mtEscuderias.addRow(fila);
            }
        } catch (SQLException sqle) {
            Util.mensajeError("Error al listar escuderias", "Escuderias");
            sqle.printStackTrace();
        }
    }

    public void listarPilotos()
    {

        mtPilotos.setNumRows(0);

        String sql = "SELECT * FROM piloto";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();

            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String nacionalidad = resultado.getString("nacionalidad");
                String escuderia = resultado.getString("escuderia");
                Date fechaNacimiento = resultado.getDate("fecha_nacimiento");
                int campeonatos = resultado.getInt("campeonatos");
                int peso = resultado.getInt("peso");
                int altura = resultado.getInt("altura");

                Object[] fila = new Object[]{id, nombre, nacionalidad,
                        escuderia, fechaNacimiento, campeonatos, peso, altura};

                mtPilotos.addRow(fila);
            }
        } catch (SQLException sqle) {
            Util.mensajeError("Error al listar pilotos", "Pilotos");
            sqle.printStackTrace();
        }
    }

    public void listarCoches()
    {
        mtCoches.setNumRows(0);

        String sql = "SELECT * FROM coche";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();

            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String motor = resultado.getString("motor");
                int caballos = resultado.getInt("caballos");
                String piloto = resultado.getString("piloto");
                String carroceria = resultado.getString("carroceria");
                float costeCoche = resultado.getFloat("coste_coche");

                Object[] fila = new Object[]{id, nombre, motor, caballos,
                        piloto, carroceria,costeCoche};

                mtCoches.addRow(fila);
            }
        } catch (SQLException sqle) {
            Util.mensajeError("Error al listar pilotos", "Pilotos");
            sqle.printStackTrace();
        }
    }
    private void añadircamposcb(){
        cbBuscarEscuderia.addItem("nombre");
        cbBuscarEscuderia.addItem("director");
        cbBuscarEscuderia.addItem("pais");
        cbBuscarEscuderia.addItem("numero_trabajadores");
        cbBuscarPiloto.addItem("nombre");
        cbBuscarPiloto.addItem("nacionalidad");
        cbBuscarPiloto.addItem("campeonatos");
        cbBuscarPiloto.addItem("peso");
        cbBuscarPiloto.addItem("altura");
        cbBuscarCoche.addItem("nombre");
        cbBuscarCoche.addItem("motor");
        cbBuscarCoche.addItem("caballos");
        cbBuscarCoche.addItem("carroceria");
        cbBuscarCoche.addItem("coste_coche");

    }

    private void limpiarcajasEsc()
    {
        tfNumTrabajadores.setText("");
        tfPais.setText("");
        tfNomDirector.setText("");
        tfNomEscuderia.setText("");
        dcFechaCreacion.setDate(null);
    }

    private void limpiarcajasPiloto()
    {
        tfAltura.setText("");
        tfNacionalidad.setText("");
        tfNombrePiloto.setText("");
        tfCampeonatos.setText("");
        dcFechanacimiento.setDate(null);
        tfPeso.setText("");
    }

    private void limpiarcajasCoche()
    {
        tfCarroceria.setText("");
        tfNombreCoche.setText("");
        tfMotor.setText("");
        tfCaballos.setText("");
        tfCosteCoche.setText("");
    }

    private void actualizarcbEscuderiasConsulta()
    {
        String sql = "SELECT nombre FROM escuderia";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();

            while (resultado.next()) {

                String nombre = resultado.getString("nombre");


                cbEscuderia.addItem(nombre);

            }
        } catch (SQLException sqle) {
            Util.mensajeError("Error al actualizar cbEscuderias", "CbEscuderias");
            sqle.printStackTrace();
        }

    }

    private void actualizarcbPilotosConsulta()
    {
        String sql = "SELECT nombre FROM piloto";

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();

            while (resultado.next()) {

                String nombre = resultado.getString("nombre");


                cbPiloto.addItem(nombre);

            }
        } catch (SQLException sqle) {
            Util.mensajeError("Error al actualizar cbPilotos", "CbPilotos");
            sqle.printStackTrace();
        }

    }


    private List<Object[]> BuscarEscuderias(String busqueda, String campo)
    {
        List<Object[]> lista;
        Object[] fila;
        String sql = "SELECT * FROM escuderia WHERE " + campo + " LIKE '%" + busqueda + "%'";
        lista = new ArrayList<>();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();

            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String director = resultado.getString("director");
                String pais = resultado.getString("pais");
                Date fechaCreacion = resultado.getDate("fecha_creacion");
                int numeroTrabajadores = resultado.getInt("numero_trabajadores");

                fila = new Object[]{id, nombre, director,
                        pais, fechaCreacion, numeroTrabajadores};

                lista.add(fila);
            }
        } catch (SQLException sqle) {
            Util.mensajeError("Error al buscar escuderias", "Escuderias");
            sqle.printStackTrace();
        }
        return lista;
    }


    private void controlarBuscarEscuderias(String busqueda){

        List<Object[]> lista = BuscarEscuderias(busqueda, (String) cbBuscarEscuderia.getSelectedItem());

        if (lista != null) {

            mtEscuderias.setNumRows(0);
            for (int i = 0; i < lista.size(); i++) {

                mtEscuderias.addRow(lista.get(i));
            }
        }
    }

    private List<Object[]> BuscarPilotos(String busqueda, String campo)
    {
        List<Object[]> lista;
        Object[] fila;
        String sql = "SELECT * FROM piloto WHERE " + campo + " LIKE '%" + busqueda + "%'";
        lista = new ArrayList<>();

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();

            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String nacionalidad = resultado.getString("nacionalidad");
                String escuderia = resultado.getString("escuderia");
                Date fechaNacimiento = resultado.getDate("fecha_nacimiento");
                int campeonatos = resultado.getInt("campeonatos");
                int peso = resultado.getInt("peso");
                int altura = resultado.getInt("altura");

                fila = new Object[]{id, nombre, nacionalidad,
                        escuderia, fechaNacimiento, campeonatos, peso, altura};

                lista.add(fila);
            }
        } catch (SQLException sqle) {
            Util.mensajeError("Error al buscar pilotos", "Pilotos");
            sqle.printStackTrace();
        }
        return lista;
    }


    private void controlarBuscarPilotos(String busqueda){

        List<Object[]> lista = BuscarPilotos(busqueda, (String) cbBuscarPiloto.getSelectedItem());

        if (lista != null) {

            mtPilotos.setNumRows(0);
            for (int i = 0; i < lista.size(); i++) {

                mtPilotos.addRow(lista.get(i));
            }
        }
    }

    private List<Object[]> BuscarCoches(String busqueda, String campo)
    {
        List<Object[]> lista;
        Object[] fila;
        String sql = "SELECT * FROM coche WHERE " + campo + " LIKE '%" + busqueda + "%'";
        lista = new ArrayList<>();

        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();

            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String motor = resultado.getString("motor");
                int caballos = resultado.getInt("caballos");
                String piloto = resultado.getString("piloto");
                String carroceria = resultado.getString("carroceria");
                float costeCoche = resultado.getFloat("coste_coche");

                fila = new Object[]{id, nombre, motor, caballos,
                        piloto, carroceria,costeCoche};

                lista.add(fila);
            }
        } catch (SQLException sqle) {
            Util.mensajeError("Error al listar pilotos", "Pilotos");
            sqle.printStackTrace();
        }

        return lista;
    }


    private void controlarBuscarCoches(String busqueda){

        List<Object[]> lista = BuscarCoches(busqueda, (String) cbBuscarCoche.getSelectedItem());

        if (lista != null) {

            mtCoches.setNumRows(0);
            for (int i = 0; i < lista.size(); i++) {

                mtCoches.addRow(lista.get(i));
            }
        }
    }
    private void setEdicionEscuderias(boolean edicion) {
        tfNomEscuderia.setEditable(edicion);
        tfNomDirector.setEditable(edicion);
        dcFechaCreacion.setEnabled(edicion);
        tfNumTrabajadores.setEditable(edicion);
        tfPais.setEditable(edicion);
        cbEscuderia.setEnabled(edicion);

        btGuardarEsc.setEnabled(edicion);
        btModificarEsc.setEnabled(edicion);
        btEliminarEsc.setEnabled(edicion);
        btNuevoEsc.setEnabled(!edicion);
    }

    private void setEdicionPilotos(boolean edicion){
        tfNombrePiloto.setEditable(edicion);
        tfNacionalidad.setEditable(edicion);
        tfPeso.setEditable(edicion);
        tfAltura.setEditable(edicion);
        dcFechanacimiento.setEnabled(edicion);
        tfCampeonatos.setEditable(edicion);
        cbEscuderia.setEnabled(edicion);

        btGuardarPiloto.setEnabled(edicion);
        btModificarPiloto.setEnabled(edicion);
        btEliminarPiloto.setEnabled(edicion);
        btNuevoPiloto.setEnabled(!edicion);
    }

    private void setEdicionCoches(boolean edicion)
    {
        tfNombreCoche.setEditable(edicion);
        tfCaballos.setEditable(edicion);
        tfCarroceria.setEditable(edicion);
        tfCosteCoche.setEditable(edicion);
        tfMotor.setEditable(edicion);
        cbPiloto.setEnabled(edicion);

        btGuardarCoche.setEnabled(edicion);
        btModificarCoche.setEnabled(edicion);
        btEliminarCoche.setEnabled(edicion);
        btNuevoCoche.setEnabled(!edicion);
    }
    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e)
    {
        controlarBuscarEscuderias(tfBuscarEscuderia.getText());

        controlarBuscarPilotos(tfBuscarPiloto.getText());

        controlarBuscarCoches(tfBuscarCoche.getText());
    }
}

